. set_env.sh

docker-compose -f gitlab-production.yml run django python manage.py migrate

docker-compose -f gitlab-production.yml run django python manage.py collectstatic

docker-compose -f gitlab-production.yml exec postgres backup
docker-compose -f gitlab-production.yml run --rm awscli upload

docker image prune -f
docker volume prune -f
docker container prune -f
