from django.conf import settings
from rest_framework.routers import DefaultRouter, SimpleRouter
from django.urls import include, re_path

from sumedha_backend.users.api.views import UserViewSet,TokenRefreshView
from sumedha_backend.subjects.api.views import SubjectViewSet,MaterialCategoryList,SubjectListFilteredByClassRoom,MaterialList
from sumedha_backend.subjects.api.views import MaterialReadList,MaterialReadProgress

from sumedha_backend.feeds.api.views import EntryListViewSet,FeedViewSet,FeedCategoryViewSet,FeedCategoryEntryList


from sumedha_backend.apis.views import ping
from django.urls import path
from rest_framework_simplejwt import views as jwt_views

if settings.DEBUG:
    router = DefaultRouter()
else:
    router = SimpleRouter()


router.register(r'entries', EntryListViewSet)
router.register(r'feeds', FeedViewSet)
router.register(r'feed', FeedCategoryViewSet)
router.register("users", UserViewSet)
router.register("subjects", SubjectViewSet)

from django.urls import path, re_path
from sumedha_backend.exams.api.views import StudentExamListAPI, ExamListAPI, ExamDetailAPI, SaveStudentsAnswer, SubmitExamAPI


urlpatterns = [
	path("student/exams/list", StudentExamListAPI.as_view()),
	path("student/exams/", ExamListAPI.as_view()),
	path("student/exams/response/save/<slug:exam>", SaveStudentsAnswer.as_view()),
	re_path(r"student/exams/(?P<slug>[\w\-]+)/$", ExamDetailAPI.as_view()),
	re_path(r"student/exams/(?P<slug>[\w\-]+)/submit/$", SubmitExamAPI.as_view()),

    path('subjects/<int:subjectid>/category/', MaterialCategoryList.as_view()),
    path('subjects/<int:classroomid>/', SubjectListFilteredByClassRoom.as_view()),
    path('subjects/category/materials/<int:categoryid>/', MaterialList.as_view()),
    path('student/materials/read/<int:materialid>/', MaterialReadList.as_view()),
    path('student/materials/read/progress/', MaterialReadProgress.as_view()),
    path('feed/category/<int:categoryid>/entrys/', FeedCategoryEntryList.as_view()),
    re_path(r'^ping/$', ping, name='ping'),
    path('token/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    #path('user/password/token/', PasswordResetAPI.as_view()),
]
app_name = "api"
urlpatterns += router.urls
