from django.contrib.auth.models import AbstractUser
from django.db.models import CharField
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from django.db import models


class User(AbstractUser):
    """Default user for sumedha.
    """
    TYPE_VALUE_MAP = {
        "Student": 5,
        "Teacher": 15,
        "Admin": 20,
        "SuperAdmin": 25,
    }
    TYPE_CHOICES = [(value, name) for name, value in TYPE_VALUE_MAP.items()]
    user_type = models.IntegerField(choices=TYPE_CHOICES, blank=True, default=0)

    #: First and last name do not cover name patterns around the globe
    name = CharField(_("Name of User"), blank=True, max_length=255)

    def get_absolute_url(self):
        """Get url for user's detail view.

        Returns:
            str: URL for user detail.

        """
        return reverse("users:detail", kwargs={"username": self.username})
