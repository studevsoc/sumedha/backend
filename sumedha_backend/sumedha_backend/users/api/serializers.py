from django.contrib.auth import get_user_model
from rest_framework import serializers
from django.shortcuts import get_list_or_404, get_object_or_404

from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework_simplejwt.settings import api_settings
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework_simplejwt.exceptions import InvalidToken, TokenError
from rest_framework_simplejwt.authentication import AUTH_HEADER_TYPES



from sumedha_backend.classrooms.models import Student,Teacher,ClassRoom

User = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    user_type_id = serializers.SerializerMethodField() # add field
    classroom_id = serializers.SerializerMethodField()

    def get_user_type_id(self,user):
        user_type_id = 404.404
        if user.user_type == 5:
            user_type_id = get_object_or_404(Student,user=user).id
        elif user.user_type == 10:
            user_type_id = get_object_or_404(Teacher,user=user).id
        return user_type_id
    def get_classroom_id(self,user):
        classroom_id = 404.404
        if user.user_type == 5:
            student = get_object_or_404(Student,user=user)
            classroom_id = student.classroom.id
        elif user.user_type == 10:
            teacher = get_list_or_404(Teacher,user=user)
            classroom_id = teacher.classroom.id
        return classroom_id

    class Meta:
        model = User
        fields = ["username", "email", "user_type", "name", "url","user_type_id","classroom_id"]

        extra_kwargs = {
            "url": {"view_name": "api:user-detail", "lookup_field": "username"}
        }




class TokenRefreshSerializer(serializers.Serializer):
    refresh = serializers.CharField()

    def validate(self, attrs):
        refresh = RefreshToken(attrs['refresh'])

        data = {'access': str(refresh.access_token)}

        if api_settings.ROTATE_REFRESH_TOKENS:
            if api_settings.BLACKLIST_AFTER_ROTATION:
                try:
                    # Attempt to blacklist the given refresh token
                    refresh.blacklist()
                except AttributeError:
                    # If blacklist app not installed, `blacklist` method will
                    # not be present
                    pass

            refresh.set_jti()
            refresh.set_exp()

            data['refresh'] = str(refresh)

        return data
