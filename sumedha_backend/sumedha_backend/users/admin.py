from django.contrib import admin
from django.contrib.auth import admin as auth_admin
from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import make_password
from sumedha_backend.users.forms import UserChangeForm, UserCreationForm

User = get_user_model()




from import_export import resources, widgets
from import_export.admin import ImportExportModelAdmin


class UserResource(resources.ModelResource):

    def before_import_row(self,row, **kwargs):
        value = row['password']
        row['password'] = make_password(value)
    class Meta:
        model = User

@admin.register(User)
class UserAdmin(auth_admin.UserAdmin,ImportExportModelAdmin):
    resource_class = UserResource
    form = UserChangeForm
    add_form = UserCreationForm
    fieldsets = (("User", {"fields": ("name","user_type",)}),) + auth_admin.UserAdmin.fieldsets
    list_display = ["username", "name", "is_superuser"]
    search_fields = ["name"]

admin.site.site_header = "Sumedha Admin"
admin.site.site_title = "Sumedha Dashboard"
admin.site.index_title = "Welcome to Sumedha Dashboard"
