from django.apps import AppConfig


class SubjectConfig(AppConfig):
    name = 'sumedha_backend.subjects'
