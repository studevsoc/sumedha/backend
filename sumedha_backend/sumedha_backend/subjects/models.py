from django.db import models


from django.contrib.auth import get_user_model


import os
from django.utils import timezone
from datetime import datetime

User = get_user_model()

class SubjectIcon(models.Model):
    icon = models.CharField(max_length=30,default='')
    def __str__(self):
        return self.icon

class SubjectColor(models.Model):
    name = models.CharField(max_length=30)
    color = models.CharField(max_length=30,default='#240b36')
    color2 = models.CharField(max_length=30,default='#c31432')
    def __str__(self):
        return self.name

class Subject(models.Model):
    name = models.CharField(max_length=60)
    description = models.CharField(max_length=60,default='')
    icon = models.ForeignKey(SubjectIcon, verbose_name="SubjectIcon",default=1,on_delete=models.CASCADE, null=True)
    color = models.ForeignKey(SubjectColor, verbose_name="SubjectColor",default=1, on_delete=models.CASCADE, null=True)
    class_rooms = models.ManyToManyField('classrooms.ClassRoom',blank=True)
    subject_pos_id = models.IntegerField(default=1)
    def __str__(self):
        return self.name

def material_directory_path(instance, filename):
    basefilename, file_extension= os.path.splitext(filename)
    timenow = timezone.now()
    return 'subject/{subject}/{basename}{time}{ext}'.format(subject=instance.category.subject.name, basename=basefilename, time=timenow.strftime("%Y%m%d%H%M%S"), ext=file_extension)

class Category(models.Model):
    name = models.CharField(max_length=60)
    def __str__(self):
        return self.name

class MaterialCategory(models.Model):
    name = models.CharField(max_length=60)
    category = models.ForeignKey(Category, verbose_name="Category", on_delete=models.CASCADE, null=True)
    subject = models.ForeignKey(Subject, verbose_name="Subject", on_delete=models.CASCADE, null=True)
    class_rooms = models.ManyToManyField('classrooms.ClassRoom',blank=True)
    category_pos_id = models.IntegerField(default=1)

    def __str__(self):
        return self.name +" "+ self.subject.name

class Material(models.Model):
    category = models.ForeignKey(MaterialCategory, verbose_name="MaterialCategory", default=1, on_delete=models.CASCADE, null=True)
    user = models.ForeignKey(User, verbose_name="User", default=1, on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=60)
    TYPE_VALUE_MAP = {
        "None": 4,
        "Video": 5,
        "Audio": 15,
        "PDF": 20,
        "PPT": 21,
    }
    TYPE_CHOICES = [(value, name) for name, value in TYPE_VALUE_MAP.items()]
    file_type = models.IntegerField(choices=TYPE_CHOICES, blank=True, default=4)
    file = models.FileField(upload_to=material_directory_path, default='file.pdf',help_text="MaterialFiles")
    file_link = models.URLField(max_length=200, blank=True, default="https://www.sumedha.ml/file/video/name.mp4")
    class_rooms = models.ManyToManyField('classrooms.ClassRoom',blank=True)
    material_pos_id = models.IntegerField(default=1)
    @property
    def property_filename(self):
        return os.path.basename(self.file.name)

    def get_absolute_url(self):
        """Get url for user's detail view.

        Returns:
            str: URL for user detail.

        """
        return reverse("users:detail", kwargs={"id": self.id})
    def __str__(self):
        return self.name


class MaterialRead(models.Model):
    material = models.ForeignKey(Material, verbose_name="Material", default=1, on_delete=models.CASCADE, null=True)
    user = models.ForeignKey(User, verbose_name="User", default=1, on_delete=models.CASCADE, null=True)
    read = models.BooleanField(default=False)
    def __str__(self):
        return self.material.name + self.user.username
