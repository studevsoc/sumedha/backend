from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin, UpdateModelMixin
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet
from rest_framework import generics
from django.shortcuts import get_list_or_404, get_object_or_404
from django.db.models import Q

from .serializers import SubjectSerializer,MaterialCategorySerializer,MaterialSerializer,MaterialReadSerializer

from sumedha_backend.subjects.models import Subject,MaterialCategory,Material, MaterialRead
from sumedha_backend.classrooms.models import ClassRoom
User = get_user_model()

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions
from django.contrib.auth.models import User
from sumedha_backend.classrooms.models import Student

class MaterialReadProgress(APIView):


    def get(self, request, format=None):
        material_count = Material.objects.count()
        user_read_count = MaterialRead.objects.filter(user=request.user).filter(read=True).count()
        data = {
            "score" : 0.0,
            "material_count":material_count,
            "user_read_count": user_read_count,
            "percentage": round((user_read_count/material_count) * 100,1),
        }
        return Response(data)
class SubjectViewSet(RetrieveModelMixin, ListModelMixin, GenericViewSet):
    serializer_class = SubjectSerializer
    queryset = Subject.objects.all()
    lookup_field = "id"

    def get_queryset(self):
        user = self.request.user
        student = get_object_or_404(Student,user=user)
        return Subject.objects.filter(Q(id__in=student.classroom.subjects.all()) | Q(class_rooms=student.classroom)).order_by('subject_pos_id')

    @action(detail=False, methods=["GET"])
    def me(self, request):
        serializer = SubjectSerializer()
        return Response(status=status.HTTP_200_OK, data=serializer.data)


class SubjectListFilteredByClassRoom(generics.ListAPIView):
    serializer_class = SubjectSerializer

    def get_queryset(self):

        classroom_id = self.kwargs['classroomid']
        class_room = get_object_or_404(ClassRoom,id=classroom_id)
        return class_room.subjects.all()

class MaterialCategoryList(generics.ListAPIView):
    serializer_class = MaterialCategorySerializer

    def get_queryset(self):
        user = self.request.user
        student = get_object_or_404(Student,user=user)
        sub_id = self.kwargs['subjectid']
        subject = get_object_or_404(Subject,id=sub_id)
        return MaterialCategory.objects.filter(subject=subject).filter(Q(id__in=student.classroom.material_categorys.all()) | Q(class_rooms=student.classroom)).order_by('category_pos_id')

class MaterialList(generics.ListAPIView):
    serializer_class = MaterialSerializer

    def get_queryset(self):
        user = self.request.user
        student = get_object_or_404(Student,user=user)
        mat_cat_id = self.kwargs['categoryid']
        materialcatogory = get_object_or_404(MaterialCategory, id=mat_cat_id)
        return Material.objects.filter(category=materialcatogory).filter(Q(id__in=student.classroom.materials.all()) | Q(class_rooms=student.classroom)).order_by('material_pos_id')

class MaterialReadList(generics.ListAPIView):
    serializer_class = MaterialReadSerializer

    def get_queryset(self):

        mat_id = self.kwargs['materialid']
        material = get_object_or_404(Material, id=mat_id)
        mat,created= MaterialRead.objects.get_or_create(user=self.request.user,material=material,read=True)
        if created:
            print('read')
        else:
            mat.read=True
            mat.save()
        return MaterialRead.objects.filter(user=self.request.user)
