from django.contrib import admin

from .models import Subject,Material,Category,MaterialCategory
from .models import SubjectIcon,SubjectColor,MaterialRead


class SubjectAdmin(admin.ModelAdmin):
    list_display = ['name','subject_pos_id','get_class_rooms']
    def get_class_rooms(self, obj):
        return "\n,".join([entry.name for entry in obj.class_rooms.all()])
    get_class_rooms.short_description = 'Class Rooms'

class MaterialAdmin(admin.ModelAdmin):
    list_display = ['name','material_pos_id','category','get_class_rooms']
    list_filter = ('category','category__subject')
    def get_class_rooms(self, obj):
        return "\n,".join([entry.name for entry in obj.class_rooms.all()])
    get_class_rooms.short_description = 'Class Rooms'

class MaterialCategoryAdmin(admin.ModelAdmin):
    list_display = ['name','category_pos_id','subject','get_class_rooms']
    list_filter = ('subject',)
    def get_class_rooms(self, obj):
        return "\n,".join([entry.name for entry in obj.class_rooms.all()])
    get_class_rooms.short_description = 'Class Rooms'
# Register your models here.
admin.site.register(Subject,SubjectAdmin)
admin.site.register(Material,MaterialAdmin)
admin.site.register(Category)
admin.site.register(MaterialCategory,MaterialCategoryAdmin)
admin.site.register(SubjectIcon)
admin.site.register(SubjectColor)
admin.site.register(MaterialRead)
