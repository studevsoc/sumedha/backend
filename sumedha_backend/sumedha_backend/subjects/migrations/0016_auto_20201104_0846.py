# Generated by Django 3.0.8 on 2020-11-04 03:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('subjects', '0015_auto_20201102_0811'),
    ]

    operations = [
        migrations.AddField(
            model_name='material',
            name='material_pos_id',
            field=models.IntegerField(default=1),
        ),
        migrations.AddField(
            model_name='materialcategory',
            name='category_pos_id',
            field=models.IntegerField(default=1),
        ),
        migrations.AddField(
            model_name='subject',
            name='subject_pos_id',
            field=models.IntegerField(default=1),
        ),
    ]
