# Generated by Django 3.0.8 on 2020-08-31 08:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('feeds', '0004_feed_category'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='entry',
            name='feeds',
        ),
        migrations.AddField(
            model_name='entry',
            name='feeds',
            field=models.ManyToManyField(related_name='entries', to='feeds.Feed'),
        ),
    ]
