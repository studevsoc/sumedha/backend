from django.conf import settings
from django.db import IntegrityError, models
from django.utils import timezone
from sumedha_backend.feeds.utils.scrape_tools import ( scrape_all_urls, current_day_url,
 parse_content_from_url, preprocess_scraped_entry_item)

from sumedha_backend.feeds.utils.feed_tools import (
    fetch_feedparser_dict, preprocess_feed_entry_item
)


class Entry(models.Model):
    link = models.URLField(max_length=800, unique=True)
    published = models.DateTimeField(default=timezone.now)
    summary = models.TextField()
    title = models.CharField(max_length=280)
    timestamp = models.DateTimeField(default=timezone.now)
    feeds = models.ManyToManyField('Feed', related_name='entries')


    class Meta:
        indexes = [
            models.Index(fields=['link']),
            models.Index(fields=['published']),
            models.Index(fields=['timestamp']),
        ]
        ordering = ['-published']

    def __str__(self):
        return self.title

class FeedCategory(models.Model):
    name = models.CharField(max_length=40)
    def __str__(self):
        return self.name

class Feed(models.Model):
    title = models.CharField(max_length=1024)
    description = models.CharField(max_length=2048, default='')
    link = models.URLField(max_length=400, null=False, unique=True)
    version = models.CharField(max_length=64)
    timestamp = models.DateTimeField(default=timezone.now)
    category = models.ForeignKey('FeedCategory',default=1, verbose_name="FeedCategory", on_delete=models.CASCADE)

    class Meta:
        indexes = [models.Index(fields=['link'])]
        ordering = ['link']

    def __str__(self):
        return f'{self.title}'

    def save(self, *args, **kwargs):
        """Overrides the built-in save method.
        For new Feed objects, this method fetches meta data
        about the feed and includes these when saving. For existing
        Feed objects, the method behaves as the original save method.
        Raises:
            TypeError: when the Feed link is not given
        """
        if not self.link:
            raise TypeError('No URL for feed provided')

        try:
            feed = Feed.objects.get(link=self.link)
        except Feed.DoesNotExist:
            self.fetch_and_set_feed_details()
        super().save(*args, **kwargs)

    def fetch_and_set_feed_details(self):
        """Fetches meta details about feed from its URL.
        The fetched values ar then assigned to
        the appropriate fields of the Feed model.
        Note that the method does not call save().
        Raises:
            TypeError: when the Feed link is not given
        """
        if not self.link:
            raise TypeError('No URL for feed provided')

        parsed_feed = fetch_feedparser_dict(feed_url=self.link)
        self.title = parsed_feed.feed.get('title', '')
        self.description = parsed_feed.feed.get('description', '')
        self.version = parsed_feed.get('version', '')

    def update_feed_entries(self):
        """Fetches a given feed's available entries.
        The method then tries to save all new entries.
        Returns:
            int: count of successfully saved entries
        """
        parsed_feed = fetch_feedparser_dict(self.link)
        saved_entries_count = 0
        old_entries_count = 0
        for feed_entry in parsed_feed.entries:
            # Check if max count is reached
            if old_entries_count >= settings.MAX_SAVED_ENTRIES_COUNT:
                break

            try:
                # Process raw entry and
                # create Entry object if it does not exist yet
                item = preprocess_feed_entry_item(feed_entry)
                entry, _ = Entry.objects.get_or_create(
                    link=item['link'],
                    defaults={k: v for k, v in item.items() if k != 'link'}
                )

                # Check existing entry is already part of current feed
                old_entry = self.entries.filter(link=entry.link)

                if old_entry.exists():
                    old_entries_count += 1
                    continue
                else:
                    self.entries.add(entry)

            except Exception as e:
                pass
            else:
                saved_entries_count += 1
                old_entries_count = 0

        return saved_entries_count

class Scraper_Feed(models.Model):
    title = models.CharField(max_length=1024)
    description = models.CharField(max_length=2048, default='')
    link = models.URLField(max_length=400, null=False,default="https://blog.forumias.com/mustread/")
    version = models.CharField(max_length=64)
    timestamp = models.DateTimeField(default=timezone.now)
    category = models.ForeignKey('FeedCategory',default=1, verbose_name="FeedCategory", on_delete=models.CASCADE)
    def __str__(self):
        return f'{self.title}'

    def update_scrap_entries(self):
        """Fetches a given feed's available entries.
        The method then tries to save all new entries.
        Returns:
            int: count of successfully saved entries
        """
        #all_days = scrape_all_urls(self.link)
        today_content_url = current_day_url(self.link)
        saved_entries_count = 0
        old_entries_count = 0

        for url in today_content_url:
            url_exists = Scraper_Feed_Entry.objects.filter(feeds__in=[self]).filter(key_url=url['href']).exists()
            if not url_exists:
                data = parse_content_from_url(category=self.category.name,
                web_url=url['href'])

                if data:
                    for entry in data:
                        try:
                            item = preprocess_scraped_entry_item(entry)
                            entry, _ = Scraper_Feed_Entry.objects.get_or_create(
                                link=item['link'],
                                defaults={k: v for k, v in item.items() if k != 'link'}
                            )
                            old_entry = self.scraper_entries.filter(link=entry.link)
                            if old_entry.exists():
                                old_entries_count += 1
                                continue
                            else:
                                self.scraper_entries.add(entry)
                        except Exception as e:
                            pass
                        saved_entries_count += 1
        return saved_entries_count

class Scraper_Feed_Entry(models.Model):
    link = models.URLField(max_length=800, unique=True)
    published = models.DateTimeField(default=timezone.now)
    summary = models.TextField()
    title = models.CharField(max_length=280)
    timestamp = models.DateTimeField(default=timezone.now)
    feeds = models.ManyToManyField('Scraper_Feed', related_name='scraper_entries')
    key_url = models.URLField(max_length=800,default="https://blog.forumias.com/")


    class Meta:
        indexes = [
            models.Index(fields=['link']),
            models.Index(fields=['published']),
            models.Index(fields=['timestamp']),
        ]
        ordering = ['-published']

    def __str__(self):
        return self.title
