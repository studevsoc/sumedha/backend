from django.contrib import admin, messages
from django.http import HttpResponseRedirect

from .forms import FeedAdminAddForm, FeedAdminChangeForm
from .models import Feed,Entry,FeedCategory, Scraper_Feed,Scraper_Feed_Entry


@admin.register(Feed)
class FeedAdmin(admin.ModelAdmin):
    list_display = ['title', 'description', 'version', 'link','category']
    form = FeedAdminAddForm

    def changeform_view(self, request, object_id=None, form_url='', extra_context=None):
        """Ensures errors are handled when submitting forms in admin.
        Adapted from Stack Overflow answer:
        https://stackoverflow.com/a/39512190
        """
        try:
            response = super().changeform_view(request, object_id, form_url, extra_context)
        except Exception as e:
            self.message_user(request, e, level=messages.ERROR)
            return HttpResponseRedirect(form_url)
        return response

    def get_form(self, request, obj=None, **kwargs):
        if obj:
            return FeedAdminChangeForm
        else:
            return super().get_form(request, obj, **kwargs)

admin.site.register(Entry)
admin.site.register(FeedCategory)
admin.site.register(Scraper_Feed)
admin.site.register(Scraper_Feed_Entry)
