from rest_framework import mixins, viewsets
from rest_framework.decorators import action
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.response import Response
from rest_framework import generics
from django.shortcuts import get_list_or_404, get_object_or_404

from sumedha_backend.feeds.api.serializers import EntrySerializer, FeedSerializer,FeedCategorySerializer
from sumedha_backend.feeds.models import Entry, Feed,FeedCategory, Scraper_Feed, Scraper_Feed_Entry
import itertools


class EntryListViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    """Lists all saved entries."""
    queryset = Entry.objects.all()
    serializer_class = EntrySerializer


class FeedViewSet(viewsets.ModelViewSet):
    """Handles list, detail, and custom view methods for Feed."""
    permission_classes = [IsAdminUser, IsAuthenticated]
    queryset = Feed.objects.all()
    serializer_class = FeedSerializer

    @action(detail=True, url_name='entries')
    def entries(self, *args, **kwargs):
        """Lists all entries associated with a given RSS feed."""
        feed = self.get_object()
        page = self.paginate_queryset(feed.entries.all())
        if page is not None:
            serializer = EntrySerializer(page, many=True)
            return self.get_paginated_response(serializer.data)

class FeedCategoryViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    """Lists all category."""
    queryset = FeedCategory.objects.all()
    serializer_class = FeedCategorySerializer

class FeedCategoryEntryList(generics.ListAPIView):
    serializer_class = EntrySerializer

    def get_queryset(self):

        category_id = self.kwargs['categoryid']
        category = get_object_or_404(FeedCategory,id=category_id)
        qset1 = Entry.objects.filter(feeds__in=Feed.objects.filter(category=category))
        qset2 =  Scraper_Feed_Entry.objects.filter(feeds__in=Scraper_Feed.objects.filter(category=category))
        return list(itertools.chain(qset1,qset2))
