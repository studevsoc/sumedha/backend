#!/usr/bin/env python3

import requests
from bs4 import BeautifulSoup




# Required entry item fields
ENTRY_ITEM_FIELDS = [
    'link',
    'key_url',
    'summary',
    'title',
]



def scrape_all_urls(web_url="https://blog.forumias.com/mustread/"):
    req = requests.get(web_url)
    soup = BeautifulSoup(req.content, 'html5lib')
    table = soup.find('div', attrs = {'class':'pf-content'})
    lister = table.findAll('ul' , attrs = {'id':'lcp_instance_0'})
    rower = table.findAll('li')
    linker = table.findAll('a')
    linker = linker[:-2]
    return linker[2:]

def current_day_url(web_url="https://blog.forumias.com/mustread/"):
    req = requests.get(web_url)
    soup = BeautifulSoup(req.content, 'html5lib')
    table = soup.find('div', attrs = {'class':'pf-content'})
    lister = table.findAll('ul' , attrs = {'id':'lcp_instance_0'})
    rower = table.findAll('li')
    linker = table.findAll('a')
    linker = linker[:-2]
    return linker[2:3]

def parse_content_from_url(web_url,category):
    day = web_url
    print(web_url)
    req = requests.get(day)
    print(category)
    soup = BeautifulSoup(req.content, 'html5lib')
    data = soup.find('div' , attrs= {'class':'pf-content'})
    data_tr = data.findAll(['h3','h5','p','span'])
    key1 = None
    key2 = None
    key3 = None
    key4 = None
    key5 = None
    for key in data_tr:
        if key.contents:
            if(key.contents[0] in ('GS Paper – 1', 'GS PAPER 1', 'GS Paper 1')):
                key1 = data_tr.index(key)
            if(key.contents[0] in ('GS Paper – 2', 'GS PAPER 2', 'GS Paper 2')):
                key2 = data_tr.index(key)
            if(key.contents[0] in ('GS Paper – 3', 'GS PAPER 3', 'GS Paper 3')):
                key3 = data_tr.index(key)
            if(key.contents[0] in ('GS Paper – 4', 'GS PAPER 4', 'GS Paper 4')):
                key4 = data_tr.index(key)
            if(key.contents[0] in ('GS Paper – 5', 'GS PAPER 5',' GS Paper 5')):
                key5 = data_tr.index(key)

    data = []
    title = None
    summary = None
    link = None
    first = -1
    last = -1
    if category == 'GS PAPER 1' and key1 != None:
        first = key1
        last = key2
    if category == 'GS PAPER 2' and key2 != None:
        first = key2
        last = key3
    if category == 'GS PAPER 3' and key3 != None:
        first = key3
        last = key4
    if category == 'GS PAPER 4' and key4 != None:
        first = key4
        last = key5
    if category == 'GS PAPER 5' and key5 != None:
        first = key5
        last = None

    for x in data_tr[first:last]:
        if(x.name == 'h3'):
            extra_title = x.text
        if x.name =='p' and ((title != None)):
            summary = x.text
            data.append({"title":title,"link":link,"summary":summary,"key_url":""})
            title = None
            summary = None
            link = None
        elif x.name == 'p' and title == None:
            if "syllabus topic" not in x.text.lower():
                title = x.text
                if extra_title:
                    title += " "
                    title += extra_title
        if x.find('a') != None:
            link = x.find('a')['href']
    return data

def preprocess_scraped_entry_item(entry_item):
    """Makes an entry item ready for saving into database.
    This function applies the following operations to the given entry item:
    1. Checks if all required fields are present
    2. Converts published date into tz-aware datetime object (UTC)
    3. Removes any HTML tags from values
    4. Excludes any unnecessary fields
    Args:
        entry_item (FeedParserDict): the entry item to be processed
    Returns:
        dict: dictionary containing the processed values
    """
    for field in ENTRY_ITEM_FIELDS:
        if field not in entry_item:
            raise TypeError('{field} not found in feed entry item')

    return {
        'link': entry_item['link'],
        'summary': entry_item['summary'],
        'title': entry_item['title'],
        'key_url': entry_item['key_url']
    }
