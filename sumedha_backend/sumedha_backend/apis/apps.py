from django.apps import AppConfig


class ApisConfig(AppConfig):
    name = 'sumedha_backend.apis'

    def ready(self):
        try:
            import sumedha_backend.apis.signals  # noqa F401
        except ImportError:
            pass
