from django.apps import AppConfig


class ExamsConfig(AppConfig):
    name = 'sumedha_backend.exams'
