from django.contrib import admin
import nested_admin
from .models import Exam, Question, Answer, ExamTaker, StudentsAnswer
from sumedha_backend.classrooms.models import Student
from sumedha_backend.users.models import User
from import_export.admin import ImportExportModelAdmin
from import_export import fields, resources
from import_export.widgets import ForeignKeyWidget

class AnswerInline(nested_admin.NestedTabularInline):
	model = Answer
	extra = 4
	max_num = 4


class QuestionInline(nested_admin.NestedTabularInline):
	model = Question
	inlines = [AnswerInline,]
	extra = 5


class ExamAdmin(nested_admin.NestedModelAdmin,ImportExportModelAdmin):
	inlines = [QuestionInline,]
	list_display = ['name','roll_out','time_limit','start_date_time','subject']
	list_filter = ['subject']
	search_fields = ('name',)

class StudentsAnswerInline(admin.TabularInline):
	model = StudentsAnswer




class ExamTakerResource(resources.ModelResource):
	class Meta:
		model = ExamTaker
		fields = ('user__name',)

class ExamTakerAdmin(ImportExportModelAdmin,admin.ModelAdmin):
	resources_class = ExamTakerResource
	inlines = [StudentsAnswerInline,]
	list_display = ['get_student','get_student_username','get_student_email',
					'get_student_batch','get_exam_subject','get_exam_classroom',
					'get_student_exam','score']
	def get_student(self, obj):
		return obj.user.name
	def get_student_username(self, obj):
		return obj.user.username
	def get_student_email(self, obj):
		return obj.user.email
	def get_student_exam(self, obj):
		return obj.exam.name
	def get_exam_subject(self, obj):
		return obj.exam.subject
	def get_exam_classroom(self, obj):
		return list(obj.exam.class_rooms.all())

	def get_student_batch(self, obj):
		if obj.user.user_type == 5:
			if Student.objects.filter(user=obj.user).exists():
				student = Student.objects.get(user=obj.user)
				return student.classroom
		else:
			return None

	get_student.admin_order_field = 'user__name'
	get_student.short_description = 'Student'

	get_student_username.admin_order_field = 'user__username'
	get_student_username.short_description = 'Username'

	get_student_email.admin_order_field = 'user__email'
	get_student_email.short_description = 'Email'

	get_student_batch.short_description = 'Student Batch'

	get_student_exam.admin_order_field = 'exam__name'
	get_student_exam.short_description = 'Exam'

	get_exam_subject.admin_order_field = 'exam__subject'
	get_exam_subject.short_description = 'Subject'

	get_exam_classroom.admin_order_field = 'exam__class_rooms'
	get_exam_classroom.short_description = 'Exam Batch(s)'

class QuestionAdmin(ImportExportModelAdmin,admin.ModelAdmin):
	pass

class AnswerAdmin(ImportExportModelAdmin,admin.ModelAdmin):
	pass

admin.site.register(Exam, ExamAdmin)
admin.site.register(Question,QuestionAdmin)
admin.site.register(Answer,AnswerAdmin)
admin.site.register(ExamTaker, ExamTakerAdmin)
admin.site.register(StudentsAnswer)
