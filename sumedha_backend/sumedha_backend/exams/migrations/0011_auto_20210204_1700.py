# Generated by Django 3.0.8 on 2021-02-04 11:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('exams', '0010_auto_20210204_1615'),
    ]

    operations = [
        migrations.AlterField(
            model_name='exam',
            name='correct_answer_score',
            field=models.DecimalField(decimal_places=2, default=1.0, max_digits=5),
        ),
    ]
