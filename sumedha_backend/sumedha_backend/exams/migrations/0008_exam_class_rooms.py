# Generated by Django 3.0.8 on 2020-11-08 10:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('classrooms', '0003_auto_20201029_1833'),
        ('exams', '0007_auto_20201107_0802'),
    ]

    operations = [
        migrations.AddField(
            model_name='exam',
            name='class_rooms',
            field=models.ManyToManyField(blank=True, to='classrooms.ClassRoom'),
        ),
    ]
