# Generated by Django 3.0.8 on 2021-02-04 10:45

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('exams', '0009_auto_20201110_1418'),
    ]

    operations = [
        migrations.AddField(
            model_name='exam',
            name='correct_answer_score',
            field=models.DecimalField(decimal_places=2, default=-1.0, max_digits=5),
        ),
        migrations.AddField(
            model_name='exam',
            name='end_date_time',
            field=models.DateTimeField(blank=True, default=django.utils.timezone.now),
        ),
        migrations.AddField(
            model_name='exam',
            name='wrong_answer_score',
            field=models.DecimalField(decimal_places=2, default=1.0, max_digits=5),
        ),
    ]
