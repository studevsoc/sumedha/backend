from sumedha_backend.exams.models import Exam, ExamTaker, Question, Answer, StudentsAnswer
from rest_framework import serializers
from django.shortcuts import get_object_or_404


class ExamListSerializer(serializers.ModelSerializer):
	questions_count = serializers.SerializerMethodField()
	class Meta:
		model = Exam
		fields = ["id", "name", "description", "file", "slug", "questions_count","time_limit"]
		read_only_fields = ["questions_count"]

	def get_questions_count(self, obj):
		return obj.question_set.all().count()


class AnswerSerializer(serializers.ModelSerializer):
	class Meta:
		model = Answer
		fields = ["id", "question", "label"]


class QuestionSerializer(serializers.ModelSerializer):
	answer_set = AnswerSerializer(many=True)

	class Meta:
		model = Question
		fields = "__all__"


class StudentsAnswerSerializer(serializers.ModelSerializer):
	class Meta:
		model = StudentsAnswer
		fields = "__all__"

	def save(self):
		request_object = self.context['request']
		exam = request_object.query_params.get('exam')
		exam_taker = ExamTaker.objects.get(user=self.context['request'].user, exam=exam)

class StudentExamListSerializer(serializers.ModelSerializer):
	completed = serializers.SerializerMethodField()
	progress = serializers.SerializerMethodField()
	questions_count = serializers.SerializerMethodField()
	score = serializers.SerializerMethodField()
	subject = serializers.SerializerMethodField()
	username = serializers.SerializerMethodField()
	date = serializers.SerializerMethodField()
	class Meta:
		model = Exam
		fields = ["id", "name", "description", "file", "slug", "questions_count", "completed","time_limit", "score", "progress","subject", "date", "username"]
		read_only_fields = ["questions_count", "completed", "progress"]

	def get_subject(self,obj):
		return obj.subject.name

	def get_username(self,obj):
		return self.context['request'].user.username
	def get_date(self,obj):
		return obj.start_date_time.date()
	def get_completed(self, obj):
		try:
			examtaker = ExamTaker.objects.get(user=self.context['request'].user, exam=obj)
			return examtaker.completed
		except ExamTaker.DoesNotExist:
			return None

	def get_progress(self, obj):
		try:
			examtaker = ExamTaker.objects.get(user=self.context['request'].user, exam=obj)
			if examtaker.completed == False:
				questions_answered = StudentsAnswer.objects.filter(exam_taker=examtaker, answer__isnull=False).count()
				total_questions = obj.question_set.all().count()
				return int(questions_answered / total_questions)
			return None
		except ExamTaker.DoesNotExist:
			return None

	def get_questions_count(self, obj):
		return obj.question_set.all().count()

	def get_score(self, obj):
		try:
			examtaker = ExamTaker.objects.get(user=self.context['request'].user, exam=obj)
			if examtaker.completed == True or examtaker.timed_out:
				return examtaker.score
			return None
		except ExamTaker.DoesNotExist:
			return None



class ExamTakerSerializer(serializers.ModelSerializer):
	studentsanswer_set = StudentsAnswerSerializer(many=True)
	score = serializers.SerializerMethodField()

	class Meta:
		model = ExamTaker
		fields = "__all__"
	def get_score(self,obj):
		return int(obj.score)

class ExamDetailSerializer(serializers.ModelSerializer):
	examtakers_set = serializers.SerializerMethodField()
	question_set = QuestionSerializer(many=True)

	class Meta:
		model = Exam
		fields = "__all__"

	def get_examtakers_set(self, obj):
		try:
			exam_taker = ExamTaker.objects.get(user=self.context['request'].user, exam=obj)
			serializer = ExamTakerSerializer(exam_taker)
			return serializer.data
		except ExamTaker.DoesNotExist:
			return None


class ExamResultSerializer(serializers.Serializer):
	examtaker= serializers.SerializerMethodField()
	def get_examtaker(self,obj):
		try:
			exam = Exam.objects.get(slug=request_object.query_params.get('slug'))
			examtaker = ExamTaker.objects.get(user=self.context['request'].user, exam=exam)
			return examtaker.id

		except ExamTaker.DoesNotExist:
			return None

	def validate(self,data):
		print('permission_classes')
		return data

	def save(self):
		print(self.validated_data['examtaker'])
		examtaker = self.validated_data['examtaker']
