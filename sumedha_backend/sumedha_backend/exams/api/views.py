from django.db.models import Q
from django.shortcuts import get_object_or_404
from rest_framework import generics, permissions, status
from rest_framework.response import Response
from sumedha_backend.exams.models import Answer, Question, Exam, ExamTaker, StudentsAnswer
from .serializers import StudentExamListSerializer, ExamDetailSerializer, ExamListSerializer, ExamResultSerializer, StudentsAnswerSerializer
from django.utils import timezone

from sumedha_backend.users.models import User
from sumedha_backend.classrooms.models import Student


class StudentExamListAPI(generics.ListAPIView):
	permission_classes = [
		permissions.IsAuthenticated
	]
	serializer_class = StudentExamListSerializer

	def get_queryset(self, *args, **kwargs):
		queryset = Exam.objects.filter(examtaker__user=self.request.user)
		query = self.request.GET.get("q")

		if query:
			queryset = queryset.filter(
				Q(name__icontains=query) |
				Q(description__icontains=query)
			).distinct()

		return queryset


class ExamListAPI(generics.ListAPIView):
	serializer_class = ExamListSerializer
	permission_classes = [
		permissions.IsAuthenticated
	]

	def get_queryset(self, *args, **kwargs):
		user = self.request.user
		student = get_object_or_404(Student,user=user)
		queryset = Exam.objects.filter(roll_out=True).filter(class_rooms=student.classroom).exclude(examtaker__user=self.request.user)
		query = self.request.GET.get("q")

		if query:
			queryset = queryset.filter(
				Q(name__icontains=query) |
				Q(description__icontains=query)
			).distinct()

		return queryset


class ExamDetailAPI(generics.RetrieveAPIView):
	serializer_class = ExamDetailSerializer
	permission_classes = [
		permissions.IsAuthenticated
	]

	def get(self, *args, **kwargs):
		slug = self.kwargs["slug"]
		exam = get_object_or_404(Exam, slug=slug)
		last_question = None
		obj, created = ExamTaker.objects.get_or_create(user=self.request.user, exam=exam)
		if created:
			taker = ExamTaker.objects.get(user=self.request.user, exam=exam)
			taker.date_started = timezone.now()
			taker.save()
			for question in Question.objects.filter(exam=exam):
				StudentsAnswer.objects.create(exam_taker=obj, question=question)
		else:
			last_question = StudentsAnswer.objects.filter(exam_taker=obj, answer__isnull=False)
			if last_question.count() > 0:
				last_question = last_question.last().question.id
			else:
				last_question = None

		return Response({'exam': self.get_serializer(exam, context={'request': self.request}).data, 'last_question_id': last_question})


class SaveStudentsAnswer(generics.UpdateAPIView):
	serializer_class = StudentsAnswerSerializer
	permission_classes = [
		permissions.IsAuthenticated
	]
	def get_queryset(self, *args, **kwargs):
		queryset = StudentsAnswer.objects.filter(exam_taker__user=self.request.user)
		query = self.request.GET.get("q")

		if query:
			queryset = queryset.filter(
				Q(name__icontains=query) |
				Q(description__icontains=query)
			).distinct()

		return queryset

	def put(self, request, *args, **kwargs):
		queryset = self.get_queryset()
		exam = get_object_or_404(Exam,slug=kwargs.get('exam'))
		examtaker = ExamTaker.objects.get(user=request.user, exam=exam)
		#examtaker_id = request.data['exam_taker']
		question_id = request.data['question']
		answer_id = request.data['answer']
		#examtaker = get_object_or_404(ExamTaker, id=examtaker_id)

		question = get_object_or_404(Question, id=question_id)
		if answer_id == -1:
			question_answer = StudentsAnswer.objects.get(exam_taker=examtaker,question=question_id)
			answer = None
		else:
			answer = get_object_or_404(Answer, id=answer_id)
		time_taken = timezone.now() - examtaker.date_started
		time_taken_minutes = time_taken.total_seconds()/60
		whole_time_taken =  timezone.now() - exam.start_date_time
		whole_time_taken_minutes = whole_time_taken.total_seconds()/60
		if examtaker.completed:
			return Response({
				"message": "This exam is already complete. you can't answer any more questions"},
				status=status.HTTP_412_PRECONDITION_FAILED
			)
		else:
			if not time_taken_minutes <= exam.time_limit and exam.enable_individual_time_limit:
				examtaker.timed_out = True
				correct_answers = 0
				for students_answer in StudentsAnswer.objects.filter(exam_taker=examtaker):
					answer = Answer.objects.get(question=students_answer.question, is_correct=True)
					if students_answer.answer == answer:
						correct_answers += exam.correct_answer_score
					elif students_answer.answer != None:
						correct_answers -= exam.wrong_answer_score
				examtaker.date_finished = timezone.now()
				examtaker.score = correct_answers
				examtaker.save()
				return Response({
				"message": "timeout"},
				status=status.HTTP_200_OK
				)
			elif not whole_time_taken_minutes <= exam.time_limit and not exam.enable_individual_time_limit:
				examtaker.timed_out = True
				correct_answers = 0
				for students_answer in StudentsAnswer.objects.filter(exam_taker=examtaker):
					answer = Answer.objects.get(question=students_answer.question, is_correct=True)
					if students_answer.answer == answer:
						correct_answers += exam.correct_answer_score
					elif students_answer.answer != None:
						correct_answers -= exam.wrong_answer_score
				examtaker.date_finished = timezone.now()
				#examtaker.score = int(correct_answers / examtaker.exam.question_set.count() * 100)
				examtaker.score = correct_answers
				examtaker.save()
				return Response({
				"message": "timeout"},
				status=status.HTTP_200_OK
				)
		obj = get_object_or_404(StudentsAnswer, exam_taker=examtaker, question=question)
		obj.answer = answer
		obj.save()
		if exam.enable_individual_time_limit:
			return Response({"message":"success","time_taken":round(time_taken_minutes,1),**self.get_serializer(obj).data})
		else:
			return Response({"message":"success","time_taken":round(time_taken_minutes,1),**self.get_serializer(obj).data})


class SubmitExamAPI(generics.GenericAPIView):
	serializer_class = ExamResultSerializer
	permission_classes = [
		permissions.IsAuthenticated
	]

	def post(self, request, *args, **kwargs):
		serializer= self.get_serializer(data=request.data)
		serializer.is_valid(raise_exception=True)
		exam = Exam.objects.get(slug=kwargs.get('slug'))
		examtaker = ExamTaker.objects.get(user=request.user, exam=exam)

		if examtaker.completed:
			return Response({
				"message": "This exam is already complete. You can't submit again"},
				status=status.HTTP_412_PRECONDITION_FAILED
			)


		examtaker.completed = True
		correct_answers = 0


		for students_answer in StudentsAnswer.objects.filter(exam_taker=examtaker):
			answer = Answer.objects.get(question=students_answer.question, is_correct=True)
			print(answer)
			print(students_answer.answer)
			if students_answer.answer == answer:
				correct_answers += exam.correct_answer_score
			elif students_answer.answer != None:
				correct_answers -= exam.wrong_answer_score
		examtaker.date_finished = timezone.now()
		examtaker.score = correct_answers
		print(examtaker.score)
		examtaker.save()

		return Response({"message":"success"},status=status.HTTP_201_CREATED)
