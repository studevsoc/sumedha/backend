from django.db import models
from django.contrib.auth import get_user_model

User = get_user_model()

class ClassRoom(models.Model):
    name = models.CharField(max_length=30)
    subjects = models.ManyToManyField('subjects.Subject',blank=True)
    materials = models.ManyToManyField('subjects.Material',blank=True)
    material_categorys = models.ManyToManyField('subjects.MaterialCategory',blank=True)

    def __str__(self):
        return self.name

class Teacher(models.Model):
    user = models.OneToOneField(User, verbose_name="User", on_delete=models.CASCADE)
    subjects = models.ManyToManyField('subjects.Subject')
    classroom = models.ManyToManyField(ClassRoom)
    materials = models.ManyToManyField('subjects.Material',blank=True)


    def __str__(self):
        return self.user.name + self.user.username

class Student(models.Model):
    user = models.OneToOneField(User, verbose_name="User", on_delete=models.CASCADE)
    classroom = models.ForeignKey(ClassRoom, verbose_name="ClassRoom", on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.user.name + self.user.username
