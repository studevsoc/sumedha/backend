from django.contrib import admin

from .models import ClassRoom,Teacher,Student
from import_export import resources, widgets
from import_export.admin import ImportExportModelAdmin
from sumedha_backend.users.models import User
from django.shortcuts import get_object_or_404
from import_export.tmp_storages import CacheStorage

# Register your models here.


class StudentResource(resources.ModelResource):

    def before_import_row(self,row, **kwargs):
        value = row['user']
        user = get_object_or_404(User,username=value)
        row['user'] = user.id
        value2 = row['classroom']
        classroom = get_object_or_404(ClassRoom,name=value2)
        row['classroom'] = classroom.id
    class Meta:
        model = Student
        tmp_storage_class = CacheStorage
class StudentAdmin(ImportExportModelAdmin,admin.ModelAdmin):
    resource_class =StudentResource
    list_display = ['user','classroom']
    list_filter = ('classroom',)
    search_fields = ['user__username']


admin.site.register(ClassRoom)
admin.site.register(Teacher)
admin.site.register(Student,StudentAdmin)
